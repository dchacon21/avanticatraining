let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let urlConfig = require('./config/url-config');
app.use(bodyParser());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", urlConfig.web);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

let auth = require('./controllers/auth')(app);
app.use('/auth', auth);
app.listen(urlConfig.appPort);
