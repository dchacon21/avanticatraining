var mysql = require('mysql');
var db = require('../config/database');
var pool = mysql.createPool(db);

function findUser(id) {
    return new Promise((resolve, reject)=>{
        pool.query('SELECT * FROM USER WHERE ID="'+id+'"', function (err, rows, fields) {
            if(err){
                reject(err);
            }
            console.log('Get user: ', rows[0]);
            resolve(rows[0]);
        });
    });
}


function insert(user) {
    user.creation_date = new Date();
    user.admin = false;
    pool.query('INSERT INTO USER SET ?',user, function (err, result) {
        if (err) throw err;
        console.log('Inserted user: ', user.id);
    });
}

module.exports.insert = insert;
module.exports.findUser = findUser;
if(db.memory){
    let user={id: 'test@avantica.net', name: 'Test User', password: 'pbkdf2$10000$d9ec828ba9a6115faf3ed05f9231f6f681e4a620c4e5fb9f5a45b99681047fdec1d387e6ed6521480eff3016f80b3637395ac013e205580d5bfd35182ca5d373$eff8b9424563f696b92cfe23ad8b7621459eac36663208ed702571dfbbe45df6d0c46133e46a86e663f2f12e16475b21708f91b72d2e43df9ff2a11003c74774'
    , type: 'PASSWORD'};
    let users={};
    users[user.id] = user;
    module.exports.insert =  (user)=> users[user.id] = user;
    module.exports.findUser = (id) => new Promise((resolve, reject)=>{resolve(users[id])});
    
}