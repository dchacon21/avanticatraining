let config = {
    appPort: 3000,
    webApp: {
        topicsUrl: 'http://localhost:8080/topics',
        resourcesUrl: 'http://localhost:8080/resources'
    },
    web: 'http://localhost:7000'
};
module.exports = config;