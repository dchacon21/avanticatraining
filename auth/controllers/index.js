let express = require('express');
let router = express.Router();

router.get('/training*', function (req, res) {
    res.render('index');
});

router.get('/', function (req, res) {
    res.redirect('/training');
});

module.exports = router;