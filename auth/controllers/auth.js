let express = require('express');
let router = express.Router();

let passport = require('passport');
let config = require('../config/oauth.js');
let urlConfig = require('../config/url-config');
let session = require('express-session');
let FacebookStrategy = require('passport-facebook').Strategy;

let users = require('../infrastructure/users');
let password = require('password-hash-and-salt');
let jwt = require('jsonwebtoken');

module.exports = function (app) {
    // serialize and deserialize
    passport.serializeUser(function (user, done) {
        done(null, user);
    });
    passport.deserializeUser(function (obj, done) {
        done(null, obj);
    });

    // config
    passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL
    },
        function (accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                return done(null, profile);
            });
        }
    ));
    app.use(session({
        name: 'server-session-id',
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());

   
    router.get('/facebook',
        passport.authenticate('facebook'));

    router.get('/facebook/return',
        passport.authenticate('facebook', { failureRedirect: '/' }),
        function (req, res) {
            console.log('req.user2: ' + req.session.id + ' ' + JSON.stringify(req.user));
            req.session.user = {id: req.user.id, name: req.user.displayName};
            users.findUser(req.user.id).then( data =>{
                if(!data){
                    let user={id: req.user.id, name: req.user.displayName, password: null, type: 'FACEBOOK'};
                    users.insert(user);
                }
            }).catch(console.log);
            res.redirect(urlConfig.web);
        });

    router.get('/me', (req, res) => {
        if(req.session.user){
            let token = jwt.sign({ user: req.session.user.id }, 'secretkey');
            res.send({anonymous: false, name: req.session.user.name, id: req.session.user.id, token: token, config: urlConfig.webApp});
        }else{
            res.send({anonymous: true, token: '', config: urlConfig.webApp});
        }
    });

    router.get('/out', (req, res) => {
        if(req.session.user){
            req.session.destroy(); 
            req.logout();
        }
        res.redirect(urlConfig.web);
    });

    router.post('/signup', (req, res) => {
        users.findUser(req.body.email).then( data =>{
            if(data){
                console.log('user already exists');
                res.redirect('/error');
            }else{
                let user={id: req.body.email, name: req.body.name, password: req.body.password, type: 'PASSWORD'};
                password(req.body.password).hash((err,hash)=>{
                    if (err) throw err;
                    user.password = hash;
                
                    users.insert(user);
                    req.session.user = {id: user.id, name: user.name};   
                    let token = jwt.sign({ user: req.session.user.id }, 'secretkey');
                    res.send({anonymous: false, name: req.session.user.name, id: req.session.user.id, token: token});

                });
            }
        }).catch(console.log);

    });

    router.post('/login', (req, res) => {
        let email = req.body.email;//req.query.email
        let pass = req.body.password;//req.query.password
        console.log("Email "+email);
        users.findUser(email).then( data =>{

            if(data){
                password(pass).verifyAgainst(data.password, function(error, verified) {
                    if(error) throw new Error('Something went wrong!');
                    if(!verified) {
                        console.log("Don't try! We got you!");
                        res.send({anonymous: true, token: ''});
                    } else {
                        console.log("Good user");
                        req.session.user = {id: data.id, name: data.name};
                        let token = jwt.sign({ user: req.session.user.id }, 'secretkey');
                        console.log("token "+token);
                        res.status(202);
                        res.send({anonymous: false, name: req.session.user.name, id: req.session.user.id, token: token, session: req.session.id});
                    }
                });
            }else{
                console.log("No data");
                res.send({anonymous: true, token: ''});
            }
        }).catch(console.log);

    });

    return router;
}   