var fallback = require('express-history-api-fallback');
var express = require('express');
var app = express();
var root = __dirname ;
app.use(express.static(__dirname + '/dist/'));
app.use(express.static(__dirname + '/public/'));
app.use( '/node_modules',express.static(__dirname + '/node_modules'));
app.use(fallback('public/index.html', { root: root }));
app.listen(88);
