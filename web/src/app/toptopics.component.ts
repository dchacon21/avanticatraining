import { Component,OnInit } from '@angular/core';
import { TopicService } from './topic.service';
@Component({
    selector: 'toptopic-table',
    templateUrl: 'home.html'
})
export class TopTopicsComponent implements OnInit {
    title = 'Top ten topics ';
    topics: any[];

    constructor(private topicService: TopicService) {

    }

    ngOnInit(): any {
        console.log("ngOnInit  top components");
        if(this.topics != null){
            console.log("top components"+this.topics.length);
        }
        setTimeout(() => this.topicService.getTopTopics().then(topics =>{
            console.log("table "+JSON.stringify(topics));
            this.topics = topics;}).catch(console.log), 1000);

        
    }
}

