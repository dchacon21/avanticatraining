import { Component, OnInit } from '@angular/core';
import { TopicService} from './topic.service';
import {Topic} from './topic.model';
import {Resource} from './resource.model';

@Component({
    selector: 'topic-table',
    templateUrl: 'resource.html'
})
export class ResourcesComponent implements OnInit {
    add = false;
    edit = false;
    resources: any[];

    selectTopics: Topic[];
    resource: Resource = { description: '', id: 0, url: '', topicId: null };
    selectedResource: Resource;


    constructor(private topicService: TopicService) {
    }

    ngOnInit(): any {
        console.log("ngOnInit resources");
        this.populateList();

        this.topicService.getTopics().then(topics => {
            this.selectTopics = topics;
            this.resource.topicId = topics[0].id;
        });
    }

    createResource(): any {
        this.topicService.createResource(this.resource).then(() => {
            this.add = false;
            this.populateList();
        });

    }

    private populateList() {
        this.topicService.getResources().then(resources => this.resources = resources).catch(console.log);
    }

    editResource(r: Resource): any {
        console.log('editResource ' + r.description);
        this.selectedResource = r;
        this.edit = true;
    }

    handleChange(state) {
        if(state=='edited'){
            this.edit = false;
        }
        
    }
}
