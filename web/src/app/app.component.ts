import { Component,OnInit } from '@angular/core';
import { TopicService } from './topic.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'my-app',
  template: `
<nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" >
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Avantica Training</a>
      </div>
      
      <div class="collapse navbar-collapse" >
        <ul class="nav navbar-nav" *ngIf="islogin()">
          <li ><a [routerLink]="['/topics']">Topics</a></li>
          <li ><a [routerLink]="['/resources']">Resources</a></li> 
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li *ngIf="!islogin()"> <a [routerLink]="['/login']" >Log in</a></li>
          <li *ngIf="!islogin()"><a [routerLink]="['/signup']" >Sign up</a></li>
          
          <li *ngIf="islogin()"><a (click)="logout()" >Log out</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      
    </nav>


  <router-outlet></router-outlet>
  
  `,
  providers: [TopicService]
})
export class AppComponent implements OnInit {
  
  //user: Auth= {token:''};

  constructor(private authServiceService: AuthService) {

  }

  ngOnInit(): any {
    console.log("ngOnInit  app ");
    this.authServiceService.me();

  }
  
  islogin(){
    return this.authServiceService.islogin();
  }

  logout(){
    this.authServiceService.logout();
  }
}
