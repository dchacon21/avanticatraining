export class Resource {
    description: string;
    id: number;
    url: string;
    topicId: number;
}