import { Injectable } from '@angular/core';
import { CanActivate,ActivatedRouteSnapshot,RouterStateSnapshot,Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class LoginAuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) { 
      let result = this.authService.islogin()
      if(!result){
        console.error('Invalid not login');
        this.router.navigate(['/login']);
      }
      return result;
  }
}