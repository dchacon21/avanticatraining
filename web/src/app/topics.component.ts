import { Component, OnInit } from '@angular/core';
import { TopicService } from './topic.service';
import { Topic } from './topic.model';
@Component({
    selector: 'my-topics',
    templateUrl: 'topics.html'
})
export class TopicsComponent implements OnInit {
    add = false;
    edit = false;
    topics: any[];
    selectedTopic: Topic;

    constructor(private topicService: TopicService) {

    }

    ngOnInit(): any {
        console.log("ngOnInit resources");
        this.init();
    }
    init() {
        this.topicService.getMyTopics().then(topics => {
            console.log("tps: " + JSON.stringify(topics));
            this.topics = topics;
        }).catch(console.log);
    }


    editTopic(t) {
        console.log("editTopic");
        this.selectedTopic = t;
        this.edit = true;
    }


    handleChange(state) {
        if (state == 'edited') {
            this.edit = false;
        } else if (state == 'added') {
            this.add = false;
        }
        this.init();
    }
}