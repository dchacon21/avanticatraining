    import { NgModule }      from '@angular/core';
    import { BrowserModule } from '@angular/platform-browser';
    import { FormsModule }   from '@angular/forms';
    import { AppComponent }  from './app.component';
    import { TopTopicsComponent } from './toptopics.component';
    import { LoginComponent } from './login.component';
    import { SignupComponent } from './signup.component';
    import { ResourcesComponent} from './resources.component';
    import { TopicsComponent} from './topics.component';
    import { EditResourceComponent} from './edit.component';
    import { AddTopicComponent} from './topic.component';
    import { AuthService } from './auth.service';
    import { LoginAuthGuard } from './loginauth.guard';
    import { HttpModule }    from '@angular/http';
    import { routing }   from './app.routes';

    @NgModule({
      imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
      ],
      declarations: [
        AppComponent,
        TopTopicsComponent,
        LoginComponent,
        SignupComponent,
        ResourcesComponent,
        TopicsComponent,
        EditResourceComponent,
        AddTopicComponent
      ],
      providers: [ AuthService, LoginAuthGuard ],
      bootstrap: [ AppComponent ]
    })
    export class AppModule { }