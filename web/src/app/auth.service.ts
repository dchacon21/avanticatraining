import { Injectable } from '@angular/core';
import { Headers, Http,Response } from '@angular/http';
import config from './config';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
    authUrl = config.authUrl;
    private token = '';
    private auth : Auth;
    private config : Config

    constructor(private http: Http) { }

    login(email:string,pass:string) {
        return this.http.post(this.authUrl+'/login',{email:email,password:pass}
        ,{withCredentials: true})
            .toPromise()
            .then(response => {let data = response.json();
                this.token = data;
                console.log('token '+ this.token);
                return data;
                })
            .catch(this.handleError);
    }


    singup(email:string,pass:string, name:string) {
        return this.http.post(this.authUrl+'/signup',{email:email,password:pass,name: name}
        ,{withCredentials: true})
            .toPromise()
            .then(response => {let data = response.json();
                this.token = data.token;
                this.auth = data;
                alert(this.auth.session);
                return data;})
            .catch(this.handleError);
    }

    me() {
        return this.http.get(this.authUrl+'/me',{withCredentials: true})
            .toPromise()
            .then(response => {let data = response.json();
                this.token = data.token;
                this.config = data.config;
                if(this.token){
                    this.http._defaultOptions.headers.set('Authorization', 'Bearer ' +this.token);
                }else{
                    this.http._defaultOptions.headers.delete('Authorization');
                }

                return data;})
            .catch(this.handleError);
    }

    logout() {
      this.token = '';
      window.location.href=this.authUrl+'/out';
    }

    islogin() {
        return this.token.length > 0;
    }

    getConfig() :Config {
        return this.config;
    }

    private handleError(error: any) {
        //console.error('An error occurred', error);
        console.log( error);
        return Promise.reject(error.message || error);
    }
}


export class Auth {
  token: string;
  id: string;
  name: string;
  session: string;
}

export class Config {
  topicsUrl: string;
  resourcesUrl: string;
}
