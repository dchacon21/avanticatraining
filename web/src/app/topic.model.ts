export class Topic {
  name: string;
  id: number;
  numberOfResources: number;
}