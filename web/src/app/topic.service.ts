import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import {Topic} from './topic.model';
import {AuthService, Config} from './auth.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class TopicService {
    constructor(private http: Http, private authService: AuthService) { }

    getTopics() {
        return this.http.get(this.getTopicsUrl())
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    getTopTopics() {
        return this.http.get(this.getTopicsUrl() + '?$top=10')
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    getMyTopics() {
        return this.http.get(this.getTopicsUrl() + '?$owner=true')
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    createTopic(topic) {
        return this.http.post(this.getTopicsUrl(), topic)
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }
    updateTopic(id: number, topic) {
        return this.http.put(this.getTopicsUrl() + '/' + id, topic)
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }


    getResources() {
        return this.http.get(this.getResourcesUrl())
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    createResource(r) {
        return this.http.post(this.getResourcesUrl(), r)
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    updateResource(id: number, r) {
        return this.http.put(this.getResourcesUrl() + '/' +  id, r)
            .toPromise()
            .then(response => { return response.json(); })
            .catch(this.handleError);
    }

    private getTopicsUrl(): string {
        return this.authService.getConfig().topicsUrl;
    }

    private getResourcesUrl(): string {
        return this.authService.getConfig().resourcesUrl;
    }


    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
