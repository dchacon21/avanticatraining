import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { TopicService } from './topic.service';
import { Resource} from './resource.model';


@Component({
    selector: 'edit-resource',
    templateUrl: 'edit-resource.html'
})
export class EditResourceComponent {
    sku: string;
    @Input()
    resource: Resource;
    @Output()
    stateChange = new EventEmitter<string>();

    constructor(private topicService: TopicService) {
    }


    onSubmit(form: any): void {
        this.topicService.updateResource(this.resource.id, { description: this.resource.description, url: this.resource.url, topicId: this.resource.topicId  }).then(() => this.stateChange.emit('edited'));
    }
}
