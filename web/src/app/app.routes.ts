import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule }  from '@angular/router';
import { TopTopicsComponent } from './toptopics.component';
import { LoginComponent } from './login.component';
import { SignupComponent } from './signup.component';
import { ResourcesComponent} from './resources.component';
import { TopicsComponent} from './topics.component';
import { LoginAuthGuard } from './loginauth.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/top',
        pathMatch: 'full'
    },
    {
        path: 'top',
        component: TopTopicsComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'topics',
        component: TopicsComponent,
        canActivate: [LoginAuthGuard]
    },
    {
        path: 'resources',
        component: ResourcesComponent,
        canActivate: [LoginAuthGuard]
    }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);