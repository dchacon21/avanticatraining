import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router'
@Component({
    selector: 'topic-signup',
    templateUrl: 'signup.html',
})
export class SignupComponent {
    email: string;
    password: string;
    name: string;
    constructor(private authService: AuthService, private router: Router) {
    }

    public singup(event){
        console.log('On click sign up');
        this.authService.singup(this.email, this.password, this.name);
        this.router.navigate(['/top']);
    }

    public goFacebook(event){
        window.location.href=this.authService.authUrl+'/facebook';
    }
}
