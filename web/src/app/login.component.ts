import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
@Component({
    selector: 'topic-login',
    templateUrl: 'login.html',
})
export class LoginComponent {
    email: string;
    password: string;

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit(): any {
        console.log("ngOnInit log in");
    }

    public login(event){
        console.log('On click login');
        this.authService.login(this.email, this.password).then(data =>{
            location.href='/top';
            }).catch(console.log);
    }

    public goFacebook(event){
        window.location.href=this.authService.authUrl+'/facebook';
    }
}
