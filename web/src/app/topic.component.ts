import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { TopicService } from './topic.service';
import { Topic} from './topic.model';


@Component({
    selector: 'topic',
    templateUrl: 'topic.html'
})
export class AddTopicComponent {
    title: string;
    @Input()
    topic: Topic;

    @Output()
    stateChange = new EventEmitter<string>();

    constructor(private topicService: TopicService) {
    }

    onSubmit(form: any): void {
        if (this.topic.id > 0) {
            this.topicService.updateTopic(this.topic.id, { name: this.topic.name }).then(() => this.stateChange.emit('edited'));
        } else {
            this.topicService.createTopic({ name: this.topic.name }).then(() => this.stateChange.emit('added'));
        }

    }

    ngOnInit(): any {
        console.log("Topic Component  init ");
        if (this.topic) {
            this.title = 'Edit Topic';
        } else {
            this.topic = new Topic();
            this.title = 'Add Topic';
        }
    }
}
