var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");


gulp.task('compile', function () {
    console.log('Compile task');
    return gulp.src(['src/**/*.ts', 'typings/**/*.ts'])
        .pipe(ts(tsProject))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
})

gulp.task('default', ['compile','browser-sync'], function () {
});

gulp.task('browser-sync', ['compile','nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:88",
        files: ["public/**/*.*","dist/**/*.*"],
        port: 7000,
    });
});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch( "src/**/**.ts",['compile']);
    gulp.watch("public/js/*.js").on('change', browserSync.reload);
    gulp.watch("public/*.html").on('change', browserSync.reload);
    gulp.watch("views/*.html").on('change', browserSync.reload);
});



gulp.task('nodemon', function (cb) {
    var started = false;
    return nodemon({
        script: 'app.js',
        env: { 'NODE_ENV': 'development' }
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        if (!started) {
            cb();
            started = true;
        }
    });
});
