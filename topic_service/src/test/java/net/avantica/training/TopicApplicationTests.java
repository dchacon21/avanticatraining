package net.avantica.training;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

public class TopicApplicationTests {

    @Before
    public void before() {
        System.out.println("Before");
    }

    @After
    public void after() {
        System.out.println("After");
    }


    @Test
    public void verifyToken() throws SignatureException, NoSuchAlgorithmException, JWTVerifyException, InvalidKeyException, IOException {
        String token ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL215ZG9tYWluLmNvbS8iLCJ1c2VyIjoiZGNoYWNvbkBhdmFudGljYS5uZXQiLCJpYXQiOjE0Njk1NTI1MjV9.JqMpmvd6fwWHSGvvX5Lw6GpbcjSWxInw2PIwZkklH5Q";
        final String secret = "secretkey";

        final JWTVerifier verifier = new JWTVerifier(secret);
        final Map<String, Object> claims = verifier.verify(token);
        System.out.println("verifyToken " + claims);
    }

    @Test
    public void createToken() throws InterruptedException {
        final String issuer = "https://mydomain.com/";
        final String secret = "secretkey";

        final long iat = System.currentTimeMillis() / 1000l; // issued at claim
        final long exp = iat + 60L; // expires claim. In this case the token expires in 60 seconds

        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("iss", issuer);
        claims.put("exp", exp);
        claims.put("user","dchacon@avantica.net");
        claims.put("iat", iat);

        final String jwt = signer.sign(claims);
        System.out.println("Token: " + jwt);

    }

}
