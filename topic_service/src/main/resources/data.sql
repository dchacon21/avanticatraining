INSERT INTO topic (id, name, created, owner) VALUES (1, 'Spring boot', '2016-07-15 17:57:32', 'test@avantica.net');
INSERT INTO topic (id, name, created, owner) VALUES (2, 'REST', '2016-07-15 17:58:00', 'test@avantica.net');
INSERT INTO topic (id, name, created, owner) VALUES (3, 'Concurrency', '2016-07-15 17:58:49', 'test@avantica.net');
INSERT INTO topic (id, name, created, owner) VALUES (4, 'Node JS', '2016-07-15 17:59:39', '774624822665615');
INSERT INTO topic (id, name, created, owner) VALUES (5, 'NoSQL', '2016-07-15 17:59:51', '774624822665615');
INSERT INTO topic (id, name, created, owner) VALUES (6, 'Angular', '2016-07-15 18:00:10', null);
INSERT INTO topic (id, name, created, owner) VALUES (7, 'Design Patterns', '2016-07-15 18:01:19', null);
INSERT INTO topic (id, name, created, owner) VALUES (8, 'SOAP', '2016-07-15 18:01:47', null);
INSERT INTO topic (id, name, created, owner) VALUES (9, 'OAUTH', '2016-07-15 18:02:05', '774624822665615');
INSERT INTO topic (id, name, created, owner) VALUES (10, 'ORM', '2016-07-15 18:03:06', null);
INSERT INTO topic (id, name, created, owner) VALUES (11, 'Testing', '2016-07-15 18:03:29', null);

INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Quick start', 'http://projects.spring.io/spring-boot/#quick-start', '2016-07-15 18:03:29', 1, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Reference Guide', 'http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/', '2016-07-15 18:09:14', 1, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('REST video', 'https://www.youtube.com/watch?v=7YcW25PHnAA', '2016-07-15 18:03:29', 2, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Java concurrency', 'https://docs.oracle.com/javase/tutorial/essential/concurrency/', '2016-07-15 18:03:29', 3, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('MYSQL Isolation levels', 'https://dev.mysql.com/doc/refman/5.7/en/innodb-transaction-isolation-levels.html', '2016-07-15 18:10:55', 3, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Node API DOC', 'https://nodejs.org/dist/latest-v6.x/docs/api/', '2016-07-15 18:12:06', 4, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Express example', 'http://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm', '2016-07-19 22:58:35', 4, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('NoSql Guide', 'http://martinfowler.com/nosql.html', '2016-07-15 18:13:58', 5, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Http module', 'https://docs.angularjs.org/api/ng/service/$http#', '2016-07-19 22:58:35', 6, 'test@avantica.net');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Microservice with spring boot', 'https://www.3pillarglobal.com/insights/building-a-microservice-architecture-with-spring-boot-and-docker-part-i', '2016-07-15 23:32:54	', 1, '774624822665615');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Build Microservice', 'https://www.infoq.com/articles/boot-microservices', '2016-07-19 22:58:39', 1, '774624822665615');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Spring boot JPA', 'http://blog.netgloo.com/2014/10/27/using-mysql-in-spring-boot-via-spring-data-jpa-and-hibernate/', '2016-07-17 09:46:28', 1, '774624822665615');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('Concurrency Patterns', 'http://es.slideshare.net/melbournepatterns/concurrency-patterns-3339510', '2016-07-17 09:46:30', 3, '774624822665615');
INSERT INTO resource (description, url, created, topic_id, owner) VALUES ('multithreading-patterns-presentation', 'http://es.slideshare.net/Annie05/multithreading-patterns-presentation', '2016-07-15 23:52:25', 3, '774624822665615');





