CREATE TABLE resource
(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(64),
    url VARCHAR(256),
    created TIMESTAMP DEFAULT 'CURRENT_TIMESTAMP' NOT NULL,
    topic_id INT(11),
    owner VARCHAR(32)
);

CREATE TABLE topic
(
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    created TIMESTAMP,
    owner VARCHAR(32)
);