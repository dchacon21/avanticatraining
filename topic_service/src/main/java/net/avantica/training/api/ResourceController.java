package net.avantica.training.api;

import net.avantica.training.domain.Resource;
import net.avantica.training.infrastructure.ResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This is the API for EndpointResource
 *
 * @author dchacon
 */
@RestController
@RequestMapping(value = "/resources", produces = "application/json")
@CrossOrigin
public class ResourceController {

    private final ResourceRepository repository;

    @Autowired
    public ResourceController(ResourceRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(path = "/{resourceId}", method = RequestMethod.GET)
    public ResponseEntity<Resource> findById(@PathVariable Long resourceId){
        return new ResponseEntity<>(repository.findOne(resourceId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Resource>> findByOwner(@AuthenticationPrincipal User credentials){
        return new ResponseEntity<>(repository.findByOwner(credentials.getUsername()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Resource> insert(@RequestBody Resource resource, @AuthenticationPrincipal User credentials){
        resource.setOwner(credentials.getUsername());
        return new ResponseEntity<>(repository.save(resource), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{resourceId}", method = RequestMethod.PUT)
    public ResponseEntity<Resource> update(@PathVariable Long resourceId, @RequestBody Resource changes){
        Resource resource = repository.findOne(resourceId);
        resource.setDescription(changes.getDescription());
        resource.setUrl(changes.getUrl());
        resource.setTopicId(changes.getTopicId());
        return new ResponseEntity<>(repository.save(resource), HttpStatus.OK);
    }
}
