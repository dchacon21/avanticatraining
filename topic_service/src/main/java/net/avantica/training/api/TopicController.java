package net.avantica.training.api;

import net.avantica.training.domain.Topic;
import net.avantica.training.infrastructure.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * This is the API for EndpointResource
 *
 * @author dchacon
 */
@RestController
@RequestMapping(value = "topics", produces = "application/json")
@CrossOrigin
public class TopicController {

    private final TopicRepository repository;

    @Autowired
    public TopicController(TopicRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(path = "/{topicId}", method = RequestMethod.GET)
    public ResponseEntity<Topic> findById(@PathVariable Long topicId){
        return new ResponseEntity<>(repository.findOne(topicId), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List> getTopics(@RequestParam(value = "$top", required = false) Integer top,
                                          @RequestParam(value = "$owner", required = false) Boolean owner,
                                          @AuthenticationPrincipal User credentials){
        if(owner != null && owner.booleanValue() == true){
            return new ResponseEntity<>(repository.findByOwner(credentials.getUsername()), HttpStatus.OK);
        }
        if(top != null){
            PageRequest page = new PageRequest(0,top, Sort.Direction.DESC,"count(r.id)");
            return new ResponseEntity<>(repository.findPage(page), HttpStatus.OK);
        }

        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }



    private HashMap<String, Object> convert(Object[] result){
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", result[0]);
        map.put("count", result[1]);
        return map;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Topic>  insert(@RequestBody Topic topic, @AuthenticationPrincipal User credentials){
        topic.setOwner(credentials.getUsername());
        return new ResponseEntity<>(repository.save(topic), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{topicId}", method = RequestMethod.PUT)
    public ResponseEntity<Topic> update(@PathVariable Long topicId, @RequestBody Topic changes){
        Topic topic = repository.findOne(topicId);
        topic.setName(changes.getName());
        return new ResponseEntity<>(repository.save(topic), HttpStatus.OK);
    }
}
