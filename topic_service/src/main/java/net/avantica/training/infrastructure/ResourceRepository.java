package net.avantica.training.infrastructure;

import net.avantica.training.domain.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository to handle data base access for resources
 *
 * @author dchacon
 */
public interface ResourceRepository extends JpaRepository<Resource, Long> {

    List<Resource> findByOwner(String owner);
}
