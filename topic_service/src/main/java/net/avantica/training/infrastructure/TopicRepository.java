package net.avantica.training.infrastructure;

import net.avantica.training.domain.Topic;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Repository to handle data base access for topics
 *
 * @author dchacon
 */
public interface TopicRepository extends JpaRepository<Topic, Long> {

    @Query("SELECT new net.avantica.training.domain.Topic(t.id, t.name, count(r.id))  FROM Topic t LEFT JOIN t.resources r GROUP BY t")
    List<Topic> findPage(Pageable pageable);

    @Query("SELECT new net.avantica.training.domain.Topic(t.id, t.name, count(r.id))  FROM Topic t LEFT JOIN t.resources r WHERE t.owner=:owner GROUP BY t")
    List<Topic> findByOwner(@Param("owner") String owner);

}
