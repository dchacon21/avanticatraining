package net.avantica.training.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Model for resources
 *
 * @author dchacon
 */
@Entity
@Table(name = "resource")
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Timestamp created;
    @NotNull
    private String description;
    @NotNull
    private String url;
    @NotNull
    private Long topicId;
    @NotNull
    private String owner;

    public Resource() {
    }

    public Resource(@JsonProperty("description") String description, @JsonProperty("url")String url,
                    @JsonProperty("topicId") Long topicId) {
        this.description = description;
        this.url = url;
        this.created = Timestamp.valueOf(LocalDateTime.now());
        this.topicId = topicId;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }
}
