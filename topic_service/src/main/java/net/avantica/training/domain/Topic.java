package net.avantica.training.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Model for topics
 *
 * @author dchacon
 */
@Entity
@Table(name = "topic")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Timestamp created;
    @NotNull
    private String name;
    @NotNull
    private String owner;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "topicId", referencedColumnName = "id")
    private List<Resource> resources;

    @Transient
    private long numberOfResources;

    /**ORM constructor*/
    public Topic() {
    }

    //@JsonCreator
    public Topic(@JsonProperty("name") String name) {
        this.name = name;
        this.created = Timestamp.valueOf(LocalDateTime.now());
    }

    public Topic(Long id,String name, long numberOfResources) {
        this.id = id;
        this.name = name;
        this.numberOfResources = numberOfResources;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumberOfResources() {
        return numberOfResources;
    }
}
