# Avantica Training App #

Install gulp and typings:
```
npm install --global gulp-cli
npm install --global typings
```

## Topic service ##
Run topic service:
```
cd topic_service
gradle bootRun
```

## Auth service ##
Install auth app:
```
cd auth
npm install
```

Run auth service:
```
node app.js
```


## Web app ##
Install web app:
```
cd web
typings install
npm install
```

Run web app(watching changes):
```
gulp watch
```

Run web app:
```
node app.js
```

For test purpose it is using in memory data, and you can test using thge following account:
```
user: test@avantica.net
pass: 123
```